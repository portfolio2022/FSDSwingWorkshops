# FSDSwing Workshops

This is the starting point for your FSDSwing Workshops Exercises and includes a NetBeans project for your code development. It may also be used from time to time to contain documentation that might be generally applicable to the use of GitLab.com/KUGitlab. To do this, you should use the method shown in 'Transfer code to your Personal Portfolio Sub-Group' below. 

## Transfer code to your Personal Portfolio sub-group

You have been provided with a 'Personal Portfolio Sub-Group' with the URL https://gitlab.com/KUGitlab/ci5105/portfolio2022/<your knumber> . The intention is that all your module code development from now on should be done in repositories in this sub-group in CI5105/Portfolio2022/<your-knumber>. This is private to yourselves and the Module Staff and not accessible by other students. 

In this 'Personal Portfolio Sub-Group' you should create a 'New Project' using 'Import Project' 'Repository by URL'. The url for this repository is https://gitlab.com/KUGitlab/ci5105/content/FSDSwingWorkshops.git which you can also get from the home page of the repository selecting 'Clone' and copying the URL from 'Clone with HTTPS'
You should not change the default project name which will be FSDSwingWorkshops.

To be able to Import this project you will need to authenticate:
Username: <your knumber>_KU   e.g K1234567_KU
Password: <Personal Access Token> e.g. glpat-GxxxxXXxxXXxxxXxXX4h 

- [ ]  [Create a project access token ](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens). **You must make a copy of this token and keep it in a secure place**. You will receive an e-mail informing you that a 'Personal Access Token' has been created but it will not contain a copy of it. 
- [ ]  [Clone your new Repository using your Personal Access Token](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-using-a-token). You can do this from an IDE (e.g. NetBeans) or from the command-line (e.g. Git Bash) 
- [ ] Use a Git development process (add, commit, push) to version manage your code.  This will be covered in the module but is [typically](https://docs.gitlab.com/ee/gitlab-basics/add-file.html)

You use your Gitlab Username (<your_knumber>_KU) and your Personal Access Token to authenticate by Https to GitLab.com/KUGitlab for the purposes of typical operations e.g. Importing Projects, Cloning and Pushing to private repositories in your sub-group.  
You will typically only need one personal access token but you can create a new one whenever you require it and delete old ones. 
For logging into Gitlab.com/KUGitlab Single Sign On (SSO) you use your Kingston University e-mail address and your Kington University password. 


### Getting started
 
For the module we will use GitLab in a different way to how it is generally used in the Software Engineering world where it is typically used to allow you to collaborate with a development team and to contribute code to a common code base.  This is not the primary goal of the use of GitLab in this module. 

Initially, in this module, it will be used for publishing progamming content in order for you to:

- [ ] Access code samples referenced in lectures or workshops

Provide starting code for:
- [ ] 'Code by Example' sessions and videos
- [ ]  Workshop Exercises
- [ ]  Module Assessment Activities

Develop application code for:
- [ ] Coursework
- [ ] Your own learning development

You should use Repositories in your Portfolio2022 sub-group as described above. From here on forward, this will allow you to develop a portfolio of content associated with this module.  This sub-group will be shared with the module teaching team and will allow you to raise issues around your code in order to clarify any code-specific problems or ideas.





***

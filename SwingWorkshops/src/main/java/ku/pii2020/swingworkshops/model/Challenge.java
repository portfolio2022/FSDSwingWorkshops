/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2020.swingworkshops.model;

import java.util.List;	 	        		 	      		      	
import java.util.ArrayList;	 	        		 	      		      	
import java.io.FileNotFoundException;	 	        		 	      		      	
import java.io.File;	 	        		 	      		      	
import java.util.Scanner;
	 	        		 	      		      	
public class Challenge	 	        		 	      		      	
{	 	        		 	      		      	
  private static List<Task> tasks = new ArrayList();	 	        		 	      		      	
	 	        		 	      		      	
  public static void readCSVFile(String filename)	 	        		 	      		      	
  {	 	        		 	      		      	
    try	 	        		 	      		      	
    {	 	        		 	      		      	
    File myFile = new File(filename);	 	        		 	      		      	
    Scanner myScanner = new Scanner(myFile);	 	        		 	      		      	
    List<Task> myTasks = new ArrayList();	 	        		 	      		      	
    while(myScanner.hasNext())	 	        		 	      		      	
    {	 	        		 	      		      	
      String[] myCSV = myScanner.nextLine().split(",");	 	        		 	      		      	
      Task myTask = new Task();	 	        		 	      		      	
      myTask.setTitle(myCSV[0]);	 	        		 	      		      	
      myTask.setPriority(Integer.parseInt(myCSV[1]));	 	        		 	      		      	
      myTask.setTargetDate(myCSV[2]);	 	        		 	      		      	
      myTasks.add(myTask);	 	        		 	      		      	
    }	 	        		 	      		      	
    myScanner.close();	 	        		 	      		      	
    Challenge.setTasks(myTasks);	 	        		 	      		      	
    } catch (FileNotFoundException e)	 	        		 	      		      	
    {	 	        		 	      		      	
      e.printStackTrace();	 	        		 	      		      	
    }	 	        		 	      		      	
  }	 	        		 	      		      	
	 	        		 	      		      	
  public static List<Task> getTasks(){	 	        		 	      		      	
     return tasks;	 	        		 	      		      	
  }	 	        		 	      		      	
	 	        		 	      		      	
  public static void setTasks(List<Task> tasks){	 	        		 	      		      	
     Challenge.tasks = tasks;	 	        		 	      		      	
  }	 	        		 	      		      	
}

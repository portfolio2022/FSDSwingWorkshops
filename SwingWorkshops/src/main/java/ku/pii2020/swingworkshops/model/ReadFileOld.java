/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ku.pii2020.swingworkshops.model;

import java.io.File;	 	        		 	      		      	
import java.io.FileNotFoundException;	 	        		 	      		      	
import java.util.Scanner;
public class ReadFileOld	        		 	      		      	
{	 	        		 	      		      	
  public static String readFile()	 	        		 	      		      	
  {	 	        		 	      		      	
    try	 	        		 	      		      	
    {	 	        		 	      		      	
      File thisFile = new File("readfile.txt");	 	        		 	      		      	
      Scanner reader = new Scanner(thisFile);	 	        		 	      		      	
      String text = "";	 	        		 	      		      	
      while (reader.hasNextLine())	 	        		 	      		      	
      {	 	        		 	      		      	
        text += reader.nextLine();	 	        		 	      		      	
        text += System.lineSeparator();	 	        		 	      		      	
      }	 	        		 	      		      	
      return text;	 	        		 	      		      	
    }	 	        		 	      		      	
    catch (FileNotFoundException e)	 	        		 	      		      	
    {	 	        		 	      		      	
      System.out.println("Where da file?");	 	        		 	      		      	
      e.printStackTrace();
      return null;
    }	 	        		 	      		      	
  }	 	        		 	      		      	
}

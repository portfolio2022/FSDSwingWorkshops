/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ku.pii2020.swingworkshops.model;

import java.awt.BorderLayout;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Shadow
 */
public class DataViewer extends JFrame{
    
    private JFrame myFrame;
    private JTextArea myText;
    
    
    public DataViewer()
    {
        this.myFrame = new JFrame("DataViewer");
        this.myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.myFrame.setSize(500,500);
        this.myFrame.setVisible(true);
        this.myFrame.setLayout(new BorderLayout());
        this.myText = new JTextArea(20,10);
        this.myText.setText(ReadFileOld.readFile());
        JScrollPane myPanel = new JScrollPane(this.myText);
        myPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  
        myPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
        this.myFrame.getContentPane().add(myPanel, BorderLayout.CENTER);
        this.myFrame.setVisible(false);
        this.myFrame.setVisible(true);
        
    }
    
    public void displayTasks()
    {
        Challenge c = new Challenge();
        c.readCSVFile("Tasks.csv");
        List<Task> myTasks = c.getTasks();
        String myStrings = "Title                      Priority                            Date" + System.lineSeparator();
        for (Task nextTask : myTasks)
        {
            String taskTitle = nextTask.getTitle();
            String taskPriority = Integer.toString(nextTask.getPriority());
            LocalDate taskDate = nextTask.getTargetDate();
            String oneTask = taskTitle + "----" + taskPriority + "----" + taskDate + System.lineSeparator();
            myStrings += oneTask;
        }
        this.myText.setText(myStrings);
        
    }
    
}
